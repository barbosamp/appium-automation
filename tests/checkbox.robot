*** Settings ***

Resource        ../resources/base.robot

# EXECUTA A KEYWORD ANTES DE CADA TESTCASE
Test Setup      Open Session
# EXECUTA A KEYWORD DEPOIS DE CADA TESTCASE
Test Teardown   Close Session

*** Test Cases ***
Deve marcar a opção Robot Framework
    Go To CheckBox

    ${element}=         Set Variable        xpath=//android.widget.CheckBox[contains(@text, 'Robot Framework')]

    Click Element                       ${element}
    Wait Until Element Is Visible       id=io.qaninja.android.twp:id/rvContainer
    Element Attribute Should Match      ${element}       checked     true