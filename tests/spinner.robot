*** Settings ***

Resource        ../resources/base.robot

# EXECUTA A KEYWORD ANTES DE CADA TESTCASE
Test Setup      Open Session
# EXECUTA A KEYWORD DEPOIS DE CADA TESTCASE
Test Teardown   Close Session

*** Variables ***
${SPINNER}=             io.qaninja.android.twp:id/spinnerJob
${LIST_OPTIONS}=        class=android.widget.ListView

*** Test Cases ***
Deve selecionar o perfil QA
    Go To SignUp Form

    Choice Job  QA

Deve selecionar o perfil DevOps
    Go To SignUp Form

    Choice Job  DevOps

*** Keywords ***
Choice Job
    [Arguments]     ${target}

    Click Element                       ${SPINNER}
    Wait Until Element Is Visible       ${LIST_OPTIONS}
    Click Text                          ${target}