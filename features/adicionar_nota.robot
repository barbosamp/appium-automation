*** Settings ***
Documentation     Escreva cenários curtos e independentes.
...    Faça uso das Tags para controle de execução

Resource    ../resources/base.robot

Test Setup        Open Session ${PLATFORM} ${SERVER}
Test Teardown     Close Session

*** Test Case ***
Cenario: Realizar busca pela home logado
  [Tags]    IOS    TEDXAA-80   REGRESSIVO   BUSCA
  Dado que estou na aplicacao
  E toco no botão flutuante '+'
  Quando criar uma nova nota
  Então devo visualizar a nova nota na lista
