*** Settings ***
Documentation       Referente aos testes de Menu

Resource            ../base.robot

*** Keywords ***
Quando acessar o menu
  Wait Until Element Is Visible     ${Menu.menu_btn}
  Click Element                     ${Menu.menu_btn}

E acessar o menu
  Wait Until Element Is Visible     ${Menu.menu_btn}
  Click Element                     ${Menu.menu_btn}

Então devo visualizar as opções do menu    
  Element Text Should Be          ${Menu.compra_button}        ${Menu.txt_comprar}
  Element Text Should Be          ${Menu.whatsapp_button}      ${Menu.txt_compre_whats}
  Element Text Should Be          ${Menu.ceaevc_button}        ${Menu.txt_ceavc}   
  Swipe Ate Texto Visivel         ${Menu.txt_atendimento}  
  Element Text Should Be          ${Menu.cartao_button}        ${Menu.card_cea}
  Element Text Should Be          ${Menu.consultar_button}     ${Menu.txt_consultar_produto}    
  Element Text Should Be          ${Menu.encontrar_button}     ${Menu.txt_encontrar_loja}        
  Element Text Should Be          ${Menu.atendimento_button}   ${Menu.txt_atendimento}

E selecionar a opção
  [Arguments]                       ${enum_menu} 
  Wait Until Page Contains          ${Menu.txt_home}           
  Click Text                        ${enum_menu}

E selecionar a opção Encontrar lojas
  Wait Until Page Contains          ${Menu.txt_home}
  Swipe Ate Texto Visivel           ${Menu.txt_encontrar_loja}
  Click Text                        ${Menu.txt_encontrar_loja}
  