*** Settings ***
Documentation       Referente a pesquisa de produtos

Resource            ../base.robot

*** Keywords ***
Quando realizar a busca de um produto
  Wait Until Page Contains Element    ${Busca.search_btn}
  Click Element                       ${Busca.search_btn}
  Input Text                          ${Busca.search_txt}    ${SEARCH.camiseta}
  Run Keyword If                    '${PLATFORM}'=='ios'         Click Element    ${Busca.btn_pesquisar}
  ...   ELSE                        Press Keycode    66

E realizar a busca de um produto
    Quando realizar a busca de um produto
    
Então devo visualizar o resultado da busca
  Wait Until Page Contains Element          ${Busca.product_img}
  Page Should Contain Element    ${Busca.product_img}

Quando realizar a busca da loja em alphaville
  Wait Until Element Is Visible       ${Busca.lupa_busca}
  Click Element                       ${Busca.lupa_busca}
  Wait Until Element Is Visible       ${Busca.selec_estado}
  Click Element                       ${Busca.selec_estado}
  Swipe Ate Texto Visivel             ${SEARCH.uf_sp}
  Click Text                          ${SEARCH.uf_sp}
  Wait Until Element Is Visible       ${Busca.selec_cidade}
  Click Element                       ${Busca.selec_cidade}
  Wait Until Page Contains            ${SEARCH.bairro}
  Click Text                          ${SEARCH.bairro}
  Wait Until Element Is Visible       ${Busca.selec_bairro}
  Click Element                       ${Busca.selec_bairro}
  Wait Until Element Is Visible       ${Busca.alphaville}
  Click Element                       ${Busca.alphaville}
  Click Element                       ${Busca.btn_pesquisar}

Então será apresentada a loja do Shopping Iguatemi
  Wait Until Element Is Visible       ${Busca.card_loja}
  Page Should Contain Text            Shopping Iguatemi Alphaville