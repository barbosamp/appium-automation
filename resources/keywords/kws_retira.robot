
*** Settings ***
Documentation       Referente aos testes de produto e descrição

Resource            ../base.robot

*** Keywords ***
Dado que o usuário adicione um produto para retirada na loja
    Dado que estou na home
    Quando adicionar um produto na sacola
    Wait Until Element Is Visible             ${Compra.txt_product}
    Wait Until Element Is Visible             ${Sacola.btn_retirar_loja}
    Click Element                             ${Sacola.btn_retirar_loja}
    Wait Until Element Is Visible             ${Compra.btn_lojas}
    Click Element                             ${Compra.btn_lojas}

    ${Result}=        Run KeyWord and Return Status        Element Should Be Visible        ${Sacola.btn_drive_bourbon}
    Run Keyword If    ${Result}               Click Element                         ${Sacola.btn_drive_bourbon}
    ...               ELSE                    Swipe Ate Elemento Visivel            ${Sacola.btn_drive_bourbon}
    Click Element                             ${Sacola.btn_drive_bourbon}

E feche o pedido na sacola para retirar na loja
    ${Result}=        Run KeyWord and Return Status        Element Should Be Visible        ${Compra.fechar_pedido_btn}
    Run Keyword If    ${Result}               Click Element                         ${Compra.fechar_pedido_btn}
    ...               ELSE                    Swipe Ate Elemento Visivel            ${Compra.fechar_pedido_btn}
    Click Element                             ${Compra.fechar_pedido_btn}

    Realizar login compra

Quando finalizar o pedido do tipo retirar na loja
    Sleep                                     5  
    Wait Until Element Is Visible             ${Compra.summary_checkout}
    Wait Until Page Contains                  ${Compra.choose_payment_form}      
    Click Text                                ${Compra.choose_payment_form}
    Wait Until Page Contains Element          ${Compra.check_boleto}     
    Click Element                             ${Compra.check_boleto}

    ${Result}=        Run KeyWord and Return Status        Element Should Be Visible        ${Compra.btn_ir_para_confirmar}
    Run Keyword If    ${Result}               Click Element                         ${Compra.btn_ir_para_confirmar}
    ...               ELSE                    Swipe Ate Elemento Visivel            ${Compra.btn_ir_para_confirmar}
    Click Element                             ${Compra.btn_ir_para_confirmar}    

    Sleep         5
    ${Result}=        Run KeyWord and Return Status        Element Should Be Visible        ${Compra.finalize_checkout}
    Run Keyword If    ${Result}               Click Element                         ${Compra.finalize_checkout}
    ...               ELSE                    Swipe Ate Elemento Visivel            ${Compra.finalize_checkout}
    Click Element                             ${Compra.finalize_checkout}

Então será apresentado endereço da loja e os dias para retirada
    Wait Until Page Contains Element          ${Compra.not_evaluate_app} 
    Click Element                             ${Compra.not_evaluate_app}
    Page Should Contain Text                  ${MSG.pedido_realizado}
    Page Should Contain Text                  ${MSG.email_Pedido_Sucesso}
    Swipe Ate Texto Visivel                   ${MSG.retirar_loja}
    Wait Until Page Contains Element          ${Compra.numero_order}
    Page Should Contain Text                  ${MSG.retirar_loja}