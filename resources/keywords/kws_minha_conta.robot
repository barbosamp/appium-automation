*** Settings ***
Documentation       Referente aos testes de edição de cadastro

Resource            ../base.robot

*** Keywords ***
Dado que estou logado na aplicacao
  Pular onboarding de entrada
  E clique em Entrar
  Quando eu realizar o login        

Quando acessar alterar meu nome
  Wait Until Element Is Visible     ${MinhaConta.edit_profile_btn}
  Click Element                     ${MinhaConta.edit_profile_btn}
  Wait Until Element Is Visible     ${MinhaConta.field_name}

E informar um novo nome  
  ${name_faker}                     Last Name
  Set Suite Variable                ${new_name}                     Teste ${name_faker}                     
  Input Text                        ${MinhaConta.field_name}                   ${new_name} 
  Wait Until Element Is Visible     ${MinhaConta.continue_btn}         
  Click Element                     ${MinhaConta.continue_btn}

Então devo visualizar o nome alterado
  Sleep     5
  Wait Until Element Is Visible     ${MinhaConta.edit_profile_btn}     
  Click Element                     ${MinhaConta.edit_profile_btn}
  Wait Until Page Contains          ${new_name}
  Page Should Contain Text          ${new_name}

Quando cadastrar um novo endereço principal    
  Wait Until Page Does Not Contain Element  ${Home.loader}
  Click Element                             ${MinhaConta.add_address_btn}
  Preencher cep
  Swipe Ate Elemento Visivel        ${MinhaConta.confir_address}
  # ${principal}=     Run KeyWord and Return Status        Element Should Be Visible        ${MinhaConta.endereco_principal_check}
  # Run Keyword If    ${principal}    Click Element                     ${MinhaConta.endereco_principal_check}
  Click Element                     ${MinhaConta.confir_address}

Quando selecionar um novo endereço na lista
    Wait Until Page Does Not Contain Element  ${Home.loader}
    Click Element                   ${div_endereco}
    ${address} =   Get Text        ${MinhaConta.field_name_reciver}        
    Set Suite Variable                ${new_addressName}        ${address}
    Swipe Ate Elemento Visivel        ${MinhaConta.confir_address}
    Click Element                     ${MinhaConta.confir_address}
    
E acesso a tela de novo endereço
  Wait Until Page Contains          ${MinhaConta.address_btn}          
  Click Text                        ${MinhaConta.address_btn}        
  Wait Until Element Is Visible     ${MinhaConta.add_address_btn}      
  Click Element                     ${MinhaConta.add_address_btn}

E acessar minha conta
    Quando acessar o menu
    Wait Until Page Contains          ${MinhaConta.minha_conta_btn}      
    Click Text                        ${MinhaConta.minha_conta_btn}

Então devo visualizar o endereço cadastrado
  Wait Until Page Contains          Endereço adicionado com sucesso!
  Page Should Contain Text          ${new_addressName}
    

Quando excluir um endereço
  Wait Until Page Contains          ${MinhaConta.address_btn}
  Click Text                        ${MinhaConta.address_btn}
  Wait Until Element Is Visible     ${MinhaConta.edit_address}
  Click Element                     ${MinhaConta.edit_address}
  Wait Until Element Is Visible     ${MinhaConta.delet_address}
  ${address} =   Get Text        ${MinhaConta.field_name_reciver}        
  Set Suite Variable                ${old_address_name}        ${address}
  Wait Until Element Is Visible     ${MinhaConta.delet_address}
  Click Element                     ${MinhaConta.delet_address}
  Wait Until Element Is Visible     ${MinhaConta.confirm_delete}
  Click Element                     ${MinhaConta.confirm_delete}

Então não devo visualizar o endereço na lista
  Sleep                                    5s
  Page Should Not Contain Text             ${old_address_name}

Quando editar um endereço
  Quando acessar o menu
  Wait Until Page Contains          ${MinhaConta.minha_conta_btn}
  Click Text                        ${MinhaConta.minha_conta_btn}
  Wait Until Page Contains          ${MinhaConta.address_btn}
  Click Text                        ${MinhaConta.address_btn}
  Wait Until Element Is Visible     ${MinhaConta.edit_address}
  Click Element                     ${MinhaConta.edit_address}
  Preencher cep
  Capture Page Screenshot
  Swipe Ate Elemento Visivel        ${MinhaConta.confir_address}
  Wait Until Element Is Visible     ${MinhaConta.confir_address}
  Click Element                     ${MinhaConta.confir_address}

Então devo visualizar o endereço alterado
  Wait Until Page Contains          Endereço atualizado com sucesso!
  Swipe Ate Texto Visivel           ${new_addressName}
  Page Should Contain Text          ${new_addressName}
  Capture Page Screenshot


## Metodo para preencher os campos de endereço
Preencher cep
  ${random_number}=                 Generate Random String    3   [NUMBERS]
  Wait Until Element Is Visible     ${MinhaConta.field_cep}
  Wait Until Page Contains Element  ${MinhaConta.field_number}
  Input Text                        ${MinhaConta.field_cep}              06449060
  Input Text                        ${MinhaConta.field_number}           10
  Set Global Variable                ${new_addressName}            Teste Automacao${random_number}
  Input Text                        ${MinhaConta.field_name_reciver}     ${new_addressName}
  ${Result}=        Run KeyWord and Return Status        Element Should Be Visible        ${btn_done}
  Run Keyword If    ${Result}               Click Element                            ${btn_done}
