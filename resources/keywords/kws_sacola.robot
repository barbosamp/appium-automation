*** Settings ***
Documentation       Referente aos testes de Bag

Resource            ../base.robot

*** Keywords ***
Quando adicionar um produto na sacola
    Quando acessar o bubble
    #Swipe Ate Elemento Visivel            ${Favoritos.product_card}
    Wait Until Page Contains Element         ${Favoritos.product_card}      
    Click Element                         ${Favoritos.product_card}
    Wait Until Page Contains Element      ${Sacola.txt_product_pdp}    
    ${detalhe_produto}=      Get Text     ${Sacola.txt_product_pdp}
    BuiltIn.Set test variable             ${desc_produto}       ${detalhe_produto}
    Swipe Ate Elemento Visivel            ${Sacola.buy_btn}
    ${Result}=    Run KeyWord And Return Status       Element Should Be Visible       ${Sacola.txt_size}
    Run Keyword If    ${Result}           Click Element                         ${Sacola.txt_size}
    Wait Until Page Contains Element      ${Sacola.buy_btn}
    Click Element                         ${Sacola.buy_btn}
    Wait Until Page Contains Element      ${Sacola.end_shopping_btn}   
    Click Element                         ${Sacola.end_shopping_btn}

Então devo visualizar o produto na sacola
    Wait Until Page Contains Element      ${Sacola.txt_size_product_bag}
    Page Should Contain Element           ${Sacola.txt_size_product_bag}
    Page Should Contain Element           ${Sacola.txt_seller}                 
    Page Should Contain Element           ${Sacola.txt_price}  

E acessar a tela de endereço de entrega no checkout
    Swipe Ate Elemento Visivel                ${Compra.fechar_pedido_btn}
    Wait Until Page Contains Element             ${Compra.fechar_pedido_btn}
    Click Element                             ${Compra.fechar_pedido_btn}
    Wait Until Page Does Not Contain Element  ${Home.loader}  
    Wait Until Page Contains Element          ${Sacola.alterar_endereco_link}
    Click Element                             ${Sacola.alterar_endereco_link}

E adicionar um produto na sacola
    Quando adicionar um produto na sacola
    Wait Until Element Is Visible             ${Compra.txt_product}
    Swipe Ate Elemento Visivel                ${Compra.fechar_pedido_btn}
    Click Element                             ${Compra.fechar_pedido_btn}