*** Settings ***
Documentation       Referente aos testes de Wishlist

Resource            ../base.robot

*** Keywords ***
E estou na PDP
  Wait Until Element Is Visible         ${Favoritos.product_card}
  ${prod} =    Get Text                 ${Favoritos.product_card}
  Set Global Variable                   ${produto}              ${prod}
  Click Element                         ${Favoritos.product_card}

Quando favoritar um produto
  Wait Until Element Is Visible         ${Favoritos.wishlist_product_btn}
  Click Element                         ${Favoritos.wishlist_product_btn}

Quando favorito o primeiro produto da lista
  Wait Until Element Is Visible         ${Favoritos.product_card}
  Swipe Ate Elemento Visivel            ${Sacola.prdct_unselected}
  Click Element                         ${Sacola.prdct_unselected}
  Wait Until Element Is Visible         ${Sacola.txt_product_pdp}
  ${prod} =    Get Text                 ${Sacola.txt_product_pdp}
  Set Global Variable                   ${produto}              ${prod}
  Capture Page Screenshot
  Wait Until Element Is Visible         ${Favoritos.wishlist_product_btn}
  Click Element                         ${Favoritos.wishlist_product_btn}
  
Swipe Ate Elemento Visivel
  [Arguments]   ${element}
  Wait Until Page Does Not Contain Element    ${Home.loader}           120
  FOR   ${i}    IN RANGE    15
    ${Result}=    Run KeyWord And Return Status       Element Should Be Enabled        ${element}
    ${Result2}=    Run KeyWord And Return Status       Page Should Contain Element        ${element}
    Exit For Loop If    ${Result}
    Exit For Loop If    ${Result2}
    Swipe   200    600    200    100    
  END

Quando desfavorito o primeiro produto favorito da lista
  Wait Until Element Is Visible         ${Favoritos.product_card}
  Swipe Ate Elemento Visivel            ${Sacola.prdct_wish_list_selected}
  Click Element                         ${Sacola.prdct_wish_list_selected}
  Wait Until Element Is Visible         ${Sacola.txt_product_pdp}
  ${prod} =    Get Text                 ${Sacola.txt_product_pdp}
  Set Global Variable                   ${produto}              ${prod}
  Capture Page Screenshot
  Wait Until Element Is Visible         ${Favoritos.wishlist_product_btn}
  Click Element                         ${Favoritos.wishlist_product_btn}

Quando acesso meus produtos favoritos
  Click Element                         ${Favoritos.wishlist_btn}
  Wait Until Element Is Visible         ${Busca.product_img}

Então devo visualizar o produto favoritado
  Wait Until Element Is Visible         ${Favoritos.heart_btn}
  Click Element                         ${Favoritos.heart_btn}
  Log To Console                        ${produto}
  Wait Until Element Is Visible         ${Favoritos.wishlist_product_label}
  Swipe Ate Texto Visivel                ${produto}
  Page Should Contain Text               ${produto}

E acesso a wishlist
  Wait Until Page Contains Element      ${Favoritos.wishlist_btn_vitrine}
  Click Element                         ${Favoritos.wishlist_btn_vitrine}

E acesse minha lista de favoritos
  Wait Until Element Is Visible         ${Favoritos.heart_btn}
  Click Element                         ${Favoritos.heart_btn}
  Capture Page Screenshot

Quando desfavoritar um determinado produto
  Wait Until Element Is Visible         ${Favoritos.wishlist_product_label}
  ${prod} =    Get Text                 ${Favoritos.wishlist_product_label}  
  Set Global Variable                   ${produto}              ${prod}
  Capture Page Screenshot
  Wait Until Element Is Visible         ${Favoritos.wishlist_heart}
  Click Element                         ${Favoritos.wishlist_heart}      
  Wait Until Element Is Visible         ${Favoritos.btn_confirmar}
  Click Element                         ${Favoritos.btn_confirmar}           

Então não verei mais esse produto na minha lista de favoritos
  Sleep                                 5
  Swipe Ate Texto Visivel               ${produto}
  Run Keyword If    ${result_exists}    Page Should Not Contain Text          ${produto}
  Capture Page Screenshot

E realize a busca de um produto favoritado
  Wait Until Page Contains Element      ${Busca.search_btn}
  Click Element                         ${Busca.search_btn}
  Input Text                          ${Busca.search_txt}    ${SEARCH.camiseta}
  Run Keyword If                    '${PLATFORM}'=='ios'         Click Element    ${Busca.btn_pesquisar}
  ...   ELSE                        Press Keycode    66

Quando desfavoritar apos o resultado da busca
  Wait Until Element Is Visible         ${Favoritos.wishlist_heart}
  Swipe Ate Elemento Visivel            ${Sacola.product_name}
  Wait Until Element Is Visible         ${Sacola.product_name}
  ${prod} =    Get Text                 ${Sacola.product_name}
  Set Global Variable                   ${produto}              ${prod}
  Click Element                         ${Favoritos.heart_selected}
  Capture Page Screenshot 
  E acesso a wishlist


E acesse a vitrine
  Wait Until Page Contains Element      ${Busca.search_btn}
  Click Element                         ${Busca.search_btn}
  Input Text                            ${Busca.search_txt}        ${SEARCH.camiseta}  
  Press Keycode                         66

E acesso a PDP de um produto favoritado
  E acesse minha lista de favoritos
  Capture Page Screenshot
  Wait Until Element Is Visible         ${Favoritos.wishlist_product_label}
  ${prod} =    Get Text                 ${Favoritos.wishlist_product_label}  
  Set Global Variable                   ${produto}              ${prod}
  Click Text                            ${produto}

Quando desfavoritar o produto na PDP
  Wait Until Element Is Visible         ${Favoritos.wishlist_product_btn}
  Click Element                         ${Favoritos.wishlist_product_btn}
  Capture Page Screenshot
  Wait Until Element Is Visible         ${Favoritos.heart_btn}
  Click Element                         ${Favoritos.heart_btn}
  Capture Page Screenshot
  