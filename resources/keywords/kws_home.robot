*** Settings ***
Documentation       Referente aos testes de Validacao na tela home

Resource            ../base.robot

*** Keywords ***
Dado que estou na home
  Pular onboarding de entrada
  Sleep                 5s
  Permissoes ios
  Click Element                     ${Home.without_account}
  Run Keyword If    '${PLATFORM}'=='android'     Insere o cep
  ${Result}=        Run KeyWord and Return Status        Page Should Contain Element        ${Compra.not_evaluate_app}
  Run Keyword If    ${Result}               Fechar Push

Então devo visualizar os principais componentes da Home
  Wait Until Element Is Visible     ${Menu.menu_btn}
  Wait Until Element Is Visible     ${Home.btn_favorito}
  Wait Until Element Is Visible     ${Home.btn_busca}
  Capture Page Screenshot
  
Fechar Push
    Click Element        ${Compra.not_evaluate_app}

Quando acessar o bubble
  Wait Until Element Is Visible     ${Home.bubble_img_home}
  Swipe Ate Texto Visivel           feminino
  Click Element                     ${Home.departamento_feminino}
  
E toco no departamento
  [Arguments]                       ${elemento_departamento}
  Wait Until Element Is Visible     ${elemento_departamento}
  Click Element                     ${elemento_departamento}
  
E toco em uma recomendação
  Wait Until Element Is Visible     ${recomendacoes}
  Click Element                     ${recomendacoes}

Então devo visualizar os produtos dessa bubble
  Wait Until Element Is Visible     ${txt_quantity}
  Wait Until Element Is Visible     ${Home.txt_item}
  ${Compra.txt_product} =   Get Text       ${Home.txt_item}
  Wait Until Page Contains          ${Compra.txt_product}

Quando selecionar um produto
  #Wait Until Element Is Visible         ${Home.bubble_img_home}
  #Click Element                         ${Home.bubble_img_home}
  Wait Until Element Is Visible         ${Favoritos.product_card}   
  Click Element                         ${Favoritos.product_card}
  Wait Until Page Contains Element      ${Sacola.txt_product_pdp}  

E seleciono um produto nao favoritado
  Swipe Ate Elemento Visivel            ${Sacola.prdct_unselected}
  Click Element                         ${Sacola.prdct_unselected}
  Wait Until Element Is Visible         ${Sacola.txt_product_pdp}
  ${prod} =    Get Text                 ${Sacola.txt_product_pdp}
  Set Global Variable                   ${produto}              ${prod}
    
E na tela de sucesso clicar em Ir para home
  Wait Until Page Contains Element          ${Compra.not_evaluate_app} 
  Click Element                             ${Compra.not_evaluate_app}
  Swipe Ate Texto Visivel               Ir para home
  Click Text                            Ir para home

Então verei a home do aplicativo
  Wait Until Element Is Visible         ${Menu.menu_btn}
  Page Should Contain Element         ${Home.logo_cea}
  Page Should Contain Element         ${Home.btn_favorito}
  Page Should Contain Element         ${Home.btn_busca}