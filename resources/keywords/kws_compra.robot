*** Settings ***
Documentation       Referente aos testes de Bag

Resource            ../base.robot

*** Keywords ***
Quando realizar uma compra de um produto logando no checkout
  Quando adicionar um produto na sacola
  Wait Until Element Is Visible             ${Compra.txt_product}
  Swipe Ate Elemento Visivel                ${Compra.fechar_pedido_btn}
  Click Element                             ${Compra.fechar_pedido_btn}
  Realizar login compra                     
  Sleep                                     5  
  Wait Until Element Is Visible             ${Compra.summary_checkout}
  Wait Until Page Contains                  ${Compra.choose_payment_form}      
  Click Text                                ${Compra.choose_payment_form}
  Sleep         2
  Wait Until Page Contains Element          ${Compra.boleto_radio}     
  Click Element                             ${Compra.boleto_radio}
  Swipe Ate Elemento Visivel                ${Compra.btn_ir_para_confirmar}
  Wait Until Page Contains Element          ${Compra.btn_ir_para_confirmar}       
  Click Element                             ${Compra.btn_ir_para_confirmar}
  Sleep         5
  Swipe Ate Elemento Visivel                ${Compra.finalize_checkout}
  Wait Until Page Contains Element          ${Compra.finalize_checkout}   
  Click Element                             ${Compra.finalize_checkout}

Quando realizar uma compra atraves de boleto
  Quando adicionar um produto na sacola
  Wait Until Page Contains Element          ${Sacola.txt_size_product_bag}
  Swipe Ate Elemento Visivel                ${Compra.fechar_pedido_btn}
  Click Element                             ${Compra.fechar_pedido_btn}
  Wait Until Element Is Visible             ${Compra.summary_checkout}
  Swipe Ate Texto Visivel                   ${Compra.choose_payment_form}     
  Click Text                                ${Compra.choose_payment_form}
  Wait Until Page Contains Element          ${Compra.boleto_radio}  
  Click Element                             ${Compra.boleto_radio}
  Sleep         2
  Swipe Ate Elemento Visivel                ${Compra.btn_ir_para_confirmar}
  Wait Until Page Contains Element          ${Compra.btn_ir_para_confirmar}       
  Click Element                             ${Compra.btn_ir_para_confirmar}
  Sleep         5
  Swipe Ate Elemento Visivel                ${Compra.finalize_checkout}
  Wait Until Page Contains Element          ${Compra.finalize_checkout}   
  Click Element                             ${Compra.finalize_checkout}

E criar um cadastro pelo checkout
  Wait Until Element Is Visible             xpath=//*[contains(@text,'Entregar')]
  Swipe Ate Elemento Visivel                ${Compra.fechar_pedido_btn}
  Click Element                             ${Compra.fechar_pedido_btn}
  ${random_number}=                 Generate Random String    4   [NUMBERS]

  Wait Until Element Is Visible     ${Cadastro.create_btn}
  Click Element                     ${Cadastro.create_btn}
  Wait Until Page Contains          ${Cadastro.field_email_create}
  Click Element                     ${Cadastro.field_email_create}
  ${email_faker}                    FakerLibrary.Email
  Set Suite Variable                ${randon_email}               cea${random_number}@automacao.eng.br
  Input Text                        ${Cadastro.field_email_create}         ${randon_email}
  Obter Token Assunto               ${randon_email} 
  Click Element                     ${Cadastro.next_create_btn}
  Preencher dados
  Selecionar check box
  Wait Until Element Is Visible     ${Cadastro.continue_btn_create}     
  Click Element                     ${Cadastro.continue_btn_create}
  

Então devo visualizar o produto comprado com sucesso
  Wait Until Page Contains Element          ${Compra.not_evaluate_app} 
  Click Element                             ${Compra.not_evaluate_app}
  Wait Until Page Contains                  Pedido realizado com sucesso.
  Swipe Ate Elemento Visivel                ${Compra.numero_order}
  ${value}=                                 Get Text        ${Compra.numero_order}
  Set Suite Variable                        ${numero_pedido}        ${value}
  Page Should Contain Element               ${Compra.numero_order}


E realizar uma compra atraves de boleto
   Quando realizar uma compra atraves de boleto

E clicar no botão Copiar Código de Barras
  Wait Until Page Contains Element          ${Compra.not_evaluate_app} 
  Click Element                             ${Compra.not_evaluate_app}
  Wait Until Page Contains                  ${MSG.pedido_realizado}
  Wait Until Page Contains                  ${MSG.email_Pedido_Sucesso}
  Click Text                                copiar código do boleto

Então devo colar o código no campo de pesquisa para validar se o botão copiar esta funcionando
  Swipe Ate Texto Visivel                   Ir para home
  Click Text                                Ir para home
  Wait Until Page Contains Element          ${Busca.search_btn}
  Click Element                             ${Busca.search_btn}
  Press Keycode                             279

E prosseguir com pagamento em boleto
    E toco no botao ir para pagamento
    Wait Until Page Does Not Contain Element  ${Home.loader}  
    Wait Until Page Contains Element          ${Compra.boleto_radio}  
    Click Element                             ${Compra.boleto_radio}
    Sleep         2
    Swipe Ate Elemento Visivel                ${Compra.btn_ir_para_confirmar}
    Wait Until Page Contains Element          ${Compra.btn_ir_para_confirmar}       
    Click Element                             ${Compra.btn_ir_para_confirmar}
    Wait Until Page Contains Element          ${Sacola.alterar_endereco_link}
    Swipe Ate Elemento Visivel                ${Compra.finalize_checkout}
    Click Element                             ${Compra.finalize_checkout}


E toco no botao ir para pagamento
    Wait Until Page Does Not Contain Element    ${Home.loader}           120
    Wait Until Page Contains Element    ${btn_ir_para_pagto}
    Click Element                       ${btn_ir_para_pagto}
    Sleep    2s

E o endereco cadastrado
    Swipe Ate Texto Visivel    ${new_addressName} 
    Wait Until Page Contains   ${new_addressName} 
    Page Should Contain Text         ${new_addressName} 
    