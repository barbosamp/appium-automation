*** Settings ***
Documentation       Referente aos testes de produto e descrição

Resource            ../base.robot

*** Keywords ***    
Entao devo visualizar o produto selecionado
    Capture Page Screenshot
    Swipe Ate Texto Visivel               R$    
    Capture Page Screenshot              
    Swipe Ate Elemento Visivel            ${Sacola.buy_btn}
    Capture Page Screenshot
    
Entao visualizo a descrição do produto
    Capture Page Screenshot
    Swipe Ate Elemento Visivel            ${Produto.descricao}
    Capture Page Screenshot