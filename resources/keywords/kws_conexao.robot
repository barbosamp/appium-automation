*** Settings ***
Documentation       Referente aos testes de Conexão de rede

Resource            ../base.robot

*** Keywords ***
E desconectar da internet
    Set Network Connection Status    0

Quando tentar acessar a tela de favoritos
    Click Element                           ${Home.btn_favorito}
    Wait Until Element Is Visible           ${Conexao.erro_conexão}  
    Capture Page Screenshot
    Wait Until Element Is Visible           ${Favoritos.entrar_conta}
    Click Element                           ${Favoritos.entrar_conta}

Então será apresentada a mensagem informando que a conexão foi perdida
    Wait Until Element Is Visible           ${Conexao.icon_ops}
    Wait Until Element Is Visible           ${Conexao.tittle_ops}
    Wait Until Element Is Visible           ${Conexao.subtittle_ops}
    Set Network Connection Status           6