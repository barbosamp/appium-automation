*** Settings ***
Documentation       Referente aos testes de Validacao do tipo de entrega

Resource            ../base.robot

*** Keywords ***
E feche o pedido na sacola para retirar na loja
    ${Result}=        Run KeyWord and Return Status        Element Should Be Visible        ${Compra.fechar_pedido_btn}
    Run Keyword If    ${Result}               Click Element                         ${Compra.fechar_pedido_btn}
    ...               ELSE                    Swipe Ate Elemento Visivel            ${Compra.fechar_pedido_btn}
    Click Element                             ${Compra.fechar_pedido_btn}
    
    Realizar login compra


Dado que o usuário adicione um produto na sacola
    Dado que estou na home
    Quando adicionar um produto na sacola
    Wait Until Element Is Visible             ${Compra.txt_product}

    ${Result}=        Run KeyWord and Return Status        Element Should Be Visible        ${Compra.fechar_pedido_btn}
    Run Keyword If    ${Result}               Click Element                         ${Compra.fechar_pedido_btn}
    ...               ELSE                    Swipe Ate Elemento Visivel            ${Compra.fechar_pedido_btn}
    Click Element                             ${Compra.fechar_pedido_btn}
    
    Realizar login compra

E seleciono o tipo de entrega
    [Arguments]                               ${tipo_entrega}
    Wait Until Element Is Visible             ${Compra.btn_tipo_entrega}
    Click Element                             ${Compra.btn_tipo_entrega}

    ${entrega}=         Set Variable          xpath=//*[contains(@text, '${tipo_entrega}')]/preceding-sibling::android.widget.ImageView[contains(@resource-id, 'ivCheckoutDeliveryOptionCheck')]
    
    Wait Until Element Is Visible             ${entrega}
    Click Element                             ${entrega}

Quando finalizar o pedido
    Click Element                             ${Compra.checkout_delivery}
    Wait Until Page Contains Element          ${Compra.check_boleto}     
    Click Element                             ${Compra.check_boleto}

    ${Result}=        Run KeyWord and Return Status        Element Should Be Visible        ${Compra.btn_ir_para_confirmar}
    Run Keyword If    ${Result}               Click Element                         ${Compra.btn_ir_para_confirmar}
    ...               ELSE                    Swipe Ate Elemento Visivel            ${Compra.btn_ir_para_confirmar}
    Click Element                             ${Compra.btn_ir_para_confirmar}    
    Sleep         5
    ${Result}=        Run KeyWord and Return Status        Element Should Be Visible        ${Compra.finalize_checkout}
    Run Keyword If    ${Result}               Click Element                         ${Compra.finalize_checkout}
    ...               ELSE                    Swipe Ate Elemento Visivel            ${Compra.finalize_checkout}
    Click Element                             ${Compra.finalize_checkout}  

Então será apresentado prazo de Entrega como 1 dia útil
    Wait Until Page Contains Element          ${Compra.not_evaluate_app} 
    Click Element                             ${Compra.not_evaluate_app}
    Page Should Contain Text                  ${MSG.pedido_realizado}
    Page Should Contain Text                  ${MSG.email_Pedido_Sucesso}
    Swipe Ate Texto Visivel                   ${MSG.entrega_expressa}
    Wait Until Page Contains Element          ${Compra.numero_order}
    Page Should Contain Text                  ${MSG.entrega_expressa}

Swipe Ate Texto Visivel
  [Arguments]                                 ${text}
  FOR   ${i}    IN RANGE    15
    ${Result}=    Run KeyWord And Return Status       Page Should Contain Text        ${text}
    Set Suite Variable                ${result_exists}               ${Result}
    Exit For Loop If    ${Result}
    Swipe   200    600    200    100    
  END

Então a compra será finalizada com sucesso
    Wait Until Page Contains Element          ${Compra.not_evaluate_app} 
    Click Element                             ${Compra.not_evaluate_app}
    Page Should Contain Text                  ${MSG.pedido_realizado}
    Page Should Contain Text                  ${MSG.email_Pedido_Sucesso}