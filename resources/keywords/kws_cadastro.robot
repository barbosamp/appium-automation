*** Settings ***
Documentation       Referente aos testes de Validacao na tela Cadastro

Resource            ../base.robot

*** Keywords ***
Quando eu realizar o cadastro
  ${random_number} =                Generate Random String    8   [NUMBERS]
  Set Suite Variable                ${email do usuario}            cea${random_number}@automacao.eng.br
  Sleep                               5s
  ${email_faker}                    FakerLibrary.Email
  Set Suite Variable                ${randon_email}               cea${random_number}@automacao.eng.br
  Input Text                        ${Cadastro.field_email_create}         ${randon_email}
  Click Element                     ${Cadastro.next_create_btn}
  Preencher dados
  Selecionar check box
  Swipe Ate Elemento Visivel        ${Cadastro.continue_btn_create}
  Wait Until Element Is Visible     ${Cadastro.continue_btn_create}   
  Click Element                     ${Cadastro.continue_btn_create}
  Obter Token Cadastro              ${randon_email} 
  Inserir token enviado por e-mail

E acesso a tela de cadastro
  Sleep             5s
  ${Result}=        Run KeyWord and Return Status        Page Should Contain Element        accessibility_id=Allow Once
  Run Keyword If    ${Result}               Click Element               accessibility_id=Allow Once
  Sleep             2s
  Wait Until Element Is Visible     ${Cadastro.create_btn}
  Click Element                     ${Cadastro.create_btn}

E acesso a tela de cadastro via checkout
  Wait Until Element Is Visible     ${Cadastro.create_btn_checkout}
  Click Element                     ${Cadastro.create_btn_checkout}

Então devo visualizar o cadastro realizado
  Wait Until Element Is Visible     ${Cadastro.botao_entrar_em_confirmação_de_cadastro}
  Page Should Contain Text          Cadastro realizado com sucesso.
  Click Element                     ${Cadastro.botao_entrar_em_confirmação_de_cadastro}

Preencher dados
  Wait Until Element Is Visible     ${Cadastro.field_name_account}
  Input Text                        ${Cadastro.field_name_account}         Teste Favorito App
  ${cpf}                            FakerLibrary.cpf
  Input Text                        ${Cadastro.field_cpf}                  ${cpf}
  Input Text                        ${Cadastro.field_phone}                ${Cadastro.phone}
  Click Element                     ${Cadastro.field_gender}
  Run Keyword If                    '${PLATFORM}'=='ios'         Seleciona genero iOS
  ...   ELSE                        Click Text                   ${gender_text}
  #Wait Until Page Contains          ${Cadastro.gender_txt}
  #Click Text                        ${Cadastro.gender_txt}
  Input Text                        ${Cadastro.field_create_password}      TesteApp1022
  Input Text                        ${Cadastro.field_confirm_password}     TesteApp1022

Seleciona genero iOS
  Click Element                     ${btn_done}

Selecionar check box
  Swipe    15    1000    15    50
  Click Element    ${Cadastro.recive_email_ckeck}
  Click Element    ${Cadastro.register_loyalty_check}

Inserir token enviado por e-mail
  Wait Until Element Is Visible  ${Cadastro.botao_cadastro_token_ok_recebi}  
  ${token cadastro} =  Obter Token Cadastro  ${email do usuario} 
 
  Click Element  ${Cadastro.botao_cadastro_token_ok_recebi}

  ${pin 1} =  Get Substring  ${token cadastro}  0  -5
  ${pin 2} =  Get Substring  ${token cadastro}  1  -4
  ${pin 3} =  Get Substring  ${token cadastro}  2  -3
  ${pin 4} =  Get Substring  ${token cadastro}  3  -2
  ${pin 5} =  Get Substring  ${token cadastro}  4  -1
  ${pin 6} =  Get Substring  ${token cadastro}  5  
  
  Wait Until Element Is Visible  ${Cadastro.campo_cadastro_token_pin_1}
  Input Text  ${Cadastro.campo_cadastro_token_pin_1}  ${pin 1}
  Input Text  ${Cadastro.campo_cadastro_token_pin_2}  ${pin 2}
  Input Text  ${Cadastro.campo_cadastro_token_pin_3}  ${pin 3}
  Input Text  ${Cadastro.campo_cadastro_token_pin_4}  ${pin 4}
  Input Text  ${Cadastro.campo_cadastro_token_pin_5}  ${pin 5}
  Input Text  ${Cadastro.campo_cadastro_token_pin_6}  ${pin 6}

  Click Element  ${Cadastro.botao_cadastro_token_validar_codigo}