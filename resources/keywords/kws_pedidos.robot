*** Settings ***
Documentation       Referente aos testes de Meus Pedidos

Resource            ../base.robot

*** Keywords ***
E finalize a compra com sucesso
    Wait Until Page Contains Element          id=rlContent                  
    Swipe                                     15    950    50    0
    Wait Until Page Contains Element          ${Compra.fechar_pedido_btn}           
    Click Element                             ${Compra.fechar_pedido_btn}
    Wait Until Page Contains                  ${Compra.choose_payment_form}        
    Click Text                                ${Compra.choose_payment_form}
    Wait Until Page Contains Element          ${Compra.boleto_radio}              
    Click Element                             ${Compra.boleto_radio}  
    Swipe                                     15    950    50    0
    Wait Until Page Contains Element          ${Favoritos.btn_confirmar}              
    Click Element                             ${Favoritos.btn_confirmar}
    Sleep         5
    Swipe                                     15    950    50    0
    Swipe                                     15    950    50    0
    Wait Until Page Contains Element          ${Compra.finalize_checkout}          
    Click Element                             ${Compra.finalize_checkout}

    #Valida a tela de Compra finalizado com sucesso
    Wait Until Page Contains Element          ${Compra.not_evaluate_app}
    Click Element                             ${Compra.not_evaluate_app}
    Wait Until Page Contains                  Pedido realizado com sucesso.
    Wait Until Page Contains                  Você receberá um e-mail com todos os detalhes. Acompanhe também seu pedido na opção Meus Pedidos dentro de “Minha Conta”.
    Wait Until Page Contains Element          ${Compra.numero_order}
    Wait Until Page Contains Element          ${Compra.numero_order}

Quando eu acessar a tela de Meus Pedidos
    Swipe Ate Elemento Visivel                ${Pedidos.btn_ir_para_pedidos}
    Click Element                             ${Pedidos.btn_ir_para_pedidos}
    Log To Console                            ${numero_pedido}
    Wait Until Page Contains                  ${numero_pedido}
    Click Element                             ${numero_pedido}
    Wait Until Page Contains                  ${numero_pedido}

Então as informações do pedido devem ser as mesmas coletadas no processo de compra
    Swipe                                     15    950    50    0
    Swipe                                     15    950    50    0
    Swipe                                     15    950    50    0
    Wait Until Page Contains                  ${desc_produto}