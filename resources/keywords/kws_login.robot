*** Settings ***
Documentation       Referente aos testes de Login

Resource            ../base.robot

# Login
*** Keywords ***
Quando eu informar um email não cadastrado 
    Wait Until Element Is Visible           ${Login.field_email}          
    Input Text                              ${Login.field_email}          ${AUTH.user_nao_cadastrado}
    Wait Until Page Contains Element        ${Login.field_password}      
    Input Text                              ${Login.field_password}       ${AUTH.password_nao_cadastrado}

E clicar em Entrar no aplicativo na tela de login
    Wait Until Page Contains Element        ${Login.entrar_app_btn}       
    Click Element                           ${Login.entrar_app_btn}

E clique em Entrar
    Sleep                                   5s
    Permissoes ios
    Wait Until Page Contains Element           ${Login.entrar_btn}        
    Click Element                           ${Login.entrar_btn}

Então devo visualizar a mensagem
    [Arguments]                             ${mensagem_erro_login}
    Wait Until Page Contains Element        ${Login.alerta_erro}
    Element Text Should Be                  ${Login.alerta_erro}             ${mensagem_erro_login}     

Dado que estou na tela pos onboarding
  Pular onboarding de entrada

Quando eu realizar o login
    Realizar login                          
  
Então devo visualizar a home do app
    Wait Until Page Contains                   ${Home.bubble_img_home}
    Page Should Contain Element                ${Home.bubble_img_home}

Quando eu informar login com senha incorreta
    Wait Until Element Is Visible           ${Login.field_email}
    Input Text                              ${Login.field_email}          ${AUTH.user_android}
    Wait Until Page Contains Element        ${Login.field_password}
    Input Text                              ${Login.field_password}       ${AUTH.senha_incorreta}

Realizar login com usuario favorito
    Wait Until Element Is Visible           ${Login.field_email}
    Input Text                              ${Login.field_email}          ${AUTH.user_favorito}
    Wait Until Page Contains Element        ${Login.field_password}
    Input Text                              ${Login.field_password}       ${AUTH.password_favorito}
    Wait Until Page Contains Element        ${Login.entrar_app_btn}
    Click Element                           ${Login.entrar_app_btn}
    Wait Until Page Does Not Contain Element    ${Home.loader}           120
    Run Keyword If    '${PLATFORM}'=='android'     Insere o cep
    ${Result}=        Run KeyWord and Return Status        Page Should Contain Element        ${Compra.not_evaluate_app}
    Run Keyword If    ${Result}               Fechar Push
     

Realizar login
    Wait Until Element Is Visible           ${Login.field_email}
    Run Keyword If    '${PLATFORM}'=='android'      Input Text        ${Login.field_email}          ${AUTH.user_android}
    ...         ELSE                                Input Text        ${Login.field_email}          ${AUTH.user_ios}
    
    Wait Until Page Contains Element        ${Login.field_password}
    Run Keyword If    '${PLATFORM}'=='android'      Input Text        ${Login.field_password}          ${AUTH.password_android}
    ...         ELSE                                Input Text        ${Login.field_password}          ${AUTH.password_ios}

    Wait Until Page Contains Element        ${Login.entrar_app_btn}
    Click Element                           ${Login.entrar_app_btn}
    Wait Until Page Does Not Contain Element    ${Home.loader}           120
    Run Keyword If    '${PLATFORM}'=='android'     Insere o cep
    ${Result}=        Run KeyWord and Return Status        Page Should Contain Element        ${Compra.not_evaluate_app}
    Run Keyword If    ${Result}               Fechar Push                  

Insere o cep
    Sleep             3s
    ${Result}=        Run KeyWord and Return Status        Element Should Be Enabled        ${Login.region_cep}
    Run Keyword If    ${Result}    Input Text        ${Login.region_cep}           ${AUTH.cep_default}
    Run Keyword If    ${Result}    Click Element                           ${Home.send_zip}
        ${Result}=        Run KeyWord and Return Status        Element Should Be Enabled        ${mais_tarde_btn}
    Run Keyword If    ${Result}    Click Element     ${mais_tarde_btn}
    
Realizar login compra
    Wait Until Element Is Visible           ${Login.field_email}
    Input Text                              ${Login.field_email}          ${AUTH.user_android}
    Wait Until Page Contains Element        ${Login.field_password}
    Input Text                              ${Login.field_password}       ${AUTH.password_android}
    Wait Until Page Contains Element        ${Login.entrar_app_btn}
    Click Element                           ${Login.entrar_app_btn}

Pular onboarding de entrada   
    ${Result}=        Run KeyWord and Return Status        Page Should Contain Element        accessibility_id=Allow
    Run Keyword If    ${Result}               Permissoes Notificacao   
    Wait Until Page Contains Element           ${Home.next_btn}
    Wait Until Page Contains Element        ${Login.txt_onboarding}      
    Click Element                           ${Home.next_btn}
    Permissoes ios
    
Dado que eu abra o aplicativo sem realizar login
    Pular onboarding de entrada
    Sleep                                   5s
    Permissoes ios
    Wait Until Page Does Not Contain Element    ${Home.loader}
    Click Element                           ${Home.without_account}
    Wait Until Page Does Not Contain Element    ${Home.loader}           120
    Run Keyword If    '${PLATFORM}'=='android'     Insere o cep
    ${Result}=        Run KeyWord and Return Status        Page Should Contain Element        ${Compra.not_evaluate_app}
    Run Keyword If    ${Result}               Fechar Push 

Permissoes ios
    ${Result}=        Run KeyWord and Return Status        Page Should Contain Element        accessibility_id=Allow Once
    Run Keyword If    ${Result}               Permissoes Localizacao
    ${Result}=        Run KeyWord and Return Status        Page Should Contain Element        accessibility_id=Allow
    Run Keyword If    ${Result}               Permissoes Notificacao
    
Permissoes Localizacao
    Click Element               accessibility_id=Allow Once

Permissoes Notificacao
    Click Element               accessibility_id=Allow

E adicione um produto a Sacola
    Quando adicionar um produto na sacola

Quando fechar o pedido
    Wait Until Page Contains Element           ${Compra.txt_product}
    Swipe Ate Elemento Visivel              ${Compra.fechar_pedido_btn}
    Click Element                           ${Compra.fechar_pedido_btn}

Então será apresentada tela para realização de login
    Wait Until Page Contains Element           ${Login.field_email}      
    Wait Until Page Contains Element           ${Login.field_password}  
               
Dado que estou logado no app com usuario favorito
  Pular onboarding de entrada
  E clique em Entrar
  Realizar login com usuario favorito 

Dado que estou logado no app
    Pular onboarding de entrada
    E clique em Entrar
    Realizar login

Quando clicar em entrar na conta
    Wait Until Element Is Visible           ${Favoritos.entrar_conta}
    Click Element                           ${Favoritos.entrar_conta}

Então realizarei o login com sucesso
    Wait Until Element Is Visible           ${Login.field_email}
    Input Text                              ${Login.field_email}          ${AUTH.user_android}
    Wait Until Page Contains Element        ${Login.field_password}
    Input Text                              ${Login.field_password}       ${AUTH.password_android}
    Wait Until Page Contains Element        ${Login.entrar_app_btn}
    Click Element                           ${Login.entrar_app_btn}
    Wait Until Element Is Visible           ${Menu.menu_btn}
    Wait Until Element Is Visible           ${Home.btn_localizacao}
    Wait Until Element Is Visible           ${Home.logo_cea}
    Wait Until Element Is Visible           ${Home.btn_favorito}
    Wait Until Element Is Visible           ${Home.btn_busca}
    Capture Page Screenshot

E clicar em C&A&VC
    Wait Until Element Is Visible           ${Menu.ceaevc_button}
    Click Element                           ${Menu.ceaevc_button}   

Quando clicar em Faça Parte
    Sleep       15
    Wait Until Element Is Visible           ${Login.btn_faca_parte}
    Click Element                           ${Login.btn_faca_parte}

E fazer login
    Wait Until Element Is Visible           ${Login.field_email}
    Input Text                              ${Login.field_email}          ${AUTH.user_android}
    Wait Until Page Contains Element        ${Login.field_password}
    Input Text                              ${Login.field_password}       ${AUTH.password_android}
    Wait Until Page Contains Element        ${Login.entrar_app_btn}
    Click Element                           ${Login.entrar_app_btn}

Então após fazer login serei direcionado para do clube C&A&VC
    Wait Until Element Is Visible           ${Menu.menu_btn}
    Click Element                           ${Menu.menu_btn}
    Wait Until Element Is Visible           ${Menu.ceaevc_button}
    Click Element                           ${Menu.ceaevc_button}
    Wait Until Page Contains Element        id=tvLoyaltyHeaderProfileImage
    
