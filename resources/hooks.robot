*** Settings ***

Resource        ../resources/base.robot

*** Keywords ***
Open Session android Browser Stack
    Set Appium Timeout      120
    Open Application        http://hub-cloud.browserstack.com/wd/hub
    ...                     browserstack.user=${BS_USER} 
    ...                     browserstack.key=${BS_KEY}
    ...                     app=${APP_URL}
    ...                     device=Google Pixel 3
    ...                     os_version=9.0
    ...                     project=C&A - Mobile Android
    ...                     build=[%{AMBIENTE} - ${PLATFORM}] - Regressivo - v3.9.0
    ...                     name=${TEST_NAME}
    ...                     autoGrantPermissions=true                       

Open Session android Local
    # Start Process                       appium
    # ...                                 alias=appiumserver
    # ...                                 stdout=logs/appium/appium_stdout.txt
    # ...                                 stderr=logs/appium/appium_stderr.txt
    # Process Should Be Running           appiumserver
    Set Appium Timeout                  60
    Open Application                    http://localhost:4723/wd/hub
    ...                                 automationName=UiAutomator2
    ...                                 platformName=Android
    ...                                 deviceName=Android Emulator
    ...                                 app=${EXECDIR}/app/${PLATFORM}/%{AMBIENTE}/twp.apk
    # ...                                 fullReset=true
    ...                                 autoGrantPermissions=true
    
Open Session ios Local
    Set Appium Timeout                  90
    Open Application                    http://localhost:4723/wd/hub
    ...                                 automationName=XCUITest
    ...                                 platformName=iOS
    ...                                 deviceName=iPhone 8 Plus
    ...                                 udid=9E6A6692-00F6-4C18-869E-1EAADBC906B1
    ...                                 app=${EXECDIR}/app/${PLATFORM}/homolog/cea.app
    ...                                 autoGrantPermissions=true
    
    

Open Session android Docker
    Remove App
    Sleep                               5s
    Set Appium Timeout                  120
    Open Application                    http://android:4723/wd/hub
    ...                                 automationName=UiAutomator2
    ...                                 platformName=Android
    ...                                 deviceName=Android Emulator
    ...                                 app=/builds/mobileb2c/automacao-app/app/${PLATFORM}/%{AMBIENTE}/cea_390.apk
    ...                                 autoGrantPermissions=true 
    ...                                 fullReset=true

Open Session ios Browser Stack
    Set Appium Timeout      120
    Open Application        http://hub-cloud.browserstack.com/wd/hub
    ...                     browserstack.user=${BS_USER} 
    ...                     browserstack.key=${BS_KEY}
    ...                     app=${APP_URL}
    ...                     device=iPhone 11
    ...                     os_version=14
    ...                     project= Regressivo CeA
    ...                     build=[%{AMBIENTE} - ${PLATFORM}] - Regressivo - v3.9.0
    ...                     name=${TEST_NAME}
    ...                     autoGrantPermissions=true 


Close Session
    Capture Page Screenshot
    Close Application

Remove App
    Remove Application    br.com.cea.appb2c.*
