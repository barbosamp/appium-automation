*** Settings ***
# Libraries
Library     AppiumLibrary
Library     Process
Library     FakerLibrary    locale=pt_BR
# Library    libs/imap.py
Library     String
Library     Collections
Library     OperatingSystem

# Keywords

Resource        keywords/kws_busca.robot

# Pages

Resource        pages/${PLATFORM}/busca_page.robot

# Data

Variables        data/credentials.yaml
Variables        data/messages.yaml
Variables        data/search.yaml

# Hooks

Resource        hooks.robot