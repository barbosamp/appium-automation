import requests

def send_simple_message():
	requests.post(
		"https://api.mailgun.net/v3/sandbox9b92cf0abd734f2a98c456a221d87afd.mailgun.org/messages",
		auth=("api", "df417391a181ab78b602abccd3349bce-77751bfc-58eaedf3"),
		data={"from": "Mailgun Sandbox <postmaster@sandbox9b92cf0abd734f2a98c456a221d87afd.mailgun.org>",
			"to": "Helder Fernandes de Paula <helder.paula@primecontrol.com.br>",
			"subject": "Hello Marcos Paulo Barbosa",
			"text": "Congratulations Marcos Paulo Barbosa, you just sent an email with Mailgun!  You are truly awesome!"})

def send_complex_message():
    return requests.post(
        "https://api.mailgun.net/v3/sandbox9b92cf0abd734f2a98c456a221d87afd.mailgun.org/messages",
        auth=("api", "df417391a181ab78b602abccd3349bce-77751bfc-58eaedf3"),
        files=[("attachment", ("report.zip", open("./logs/report.zip","rb").read()))],
        data={"from": "Mailgun Sandbox <postmaster@sandbox9b92cf0abd734f2a98c456a221d87afd.mailgun.org>",
              "to": "Helder Fernandes de Paula <helder.paula@primecontrol.com.br>",
              "subject": "Hello",
              "text": "Testing some Mailgun awesomness!",
              "html": "<html>HTML version of the body</html>"})

print(send_complex_message())