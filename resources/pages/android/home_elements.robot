*** Settings ***
Documentation       Dicionário utilizado para mapear 
...    os elementos referentes a pagina home do app.
...    - Utilizar o padrão para nomear os elementos:
...    Botões - botao_nome
...    Label  - label_nome
...    Input  - input_nome
...    Checkbox - check_nome
...    Radio Button - radio_nome
...    Link   - link_nome

Resource      ../../base.robot

*** Variables ***
&{Home}
...        botao_ok=button1
...        tolltip_adicionar=fab
...        input_name=dialog_input
...        botao_create=button1