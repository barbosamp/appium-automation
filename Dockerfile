FROM python:3

WORKDIR /usr/src/

COPY ./resources/libs/TestStatusChecker.py ./
COPY ./logs/report.zip ./
RUN pip install requests

CMD [ "python", "TestStatusChecker.py" ]